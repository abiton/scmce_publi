# Two sequential and independent pathways of erythromyeloid progenitor commitment in their niche of emergence. Lorea Iturri; Laina Freyer; Anne Biton; Pascal Dardenne; Yvan Lallemand; Elisa Gomez Perdiguero.


This repository contains the code used for the analyses of the scRNASeq data described in this manuscript under /src/YS.  



# Overlapping Definitive Progenitor Waves Cooperate to Build a Layered Hematopoietic System. Laina Freyer, Lorea Iturri, Anne Biton, Alina Sommer, Pascal Dardenne, Ana Cumano and Elisa Gomez Perdiguero.    


This repository contains the code used for the analyses of the scRNASeq data described in this manuscript under /src/FL.



# Project organisation

Raw UMI count (before any filtering step) and cell annotation data are available in /data/raw for the yolk sac ("YS_").   
UMI counts and cell annotations for both yolk sac and fetal liver are also available at https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE166223.   
Data generated throughout the scripts will be saved under /data/derived.    



