---
title: "Load Yolk Sac samples from the three different library runs"
author: "Anne Biton"
date: "`r format(Sys.time(), '%d %B, %Y')`"

output:
  prettydoc::html_pretty:
    highlight: github
    number_sections: yes
    theme: cayman
    toc: yes
---


```{r setup, eval=TRUE, echo=FALSE, warning = FALSE, message=FALSE, results=FALSE, prompt=FALSE}
knitr::opts_chunk$set(echo = FALSE, cache = TRUE, warning = FALSE, message=FALSE, results=FALSE, prompt=FALSE, dpi=150)

library(data.table)
library(ggplot2)
library(foreach)
library(BiocParallel)

library(Seurat)
library(Matrix)
library(pheatmap)
library(dplyr)
library(scales)

library(tidyverse)
library(SingleCellExperiment) 
library(scater)
library(scran) 
library(edgeR) 
library(randomcoloR)

dirdata <- "../../data/"
```

# Load data

## All runs


```{r read saved data }
annot <- readRDS(paste0(dirdata,'raw/YS_annot.rds'))
umicounts1 <- readRDS(paste0(dirdata,'raw/YS_umicounts1.rds'))
umicounts2 <- readRDS(paste0(dirdata,'raw/YS_umicounts2.rds'))
umicounts <- cbind(umicounts1, umicounts2[,-1])
rm(umicounts1,umicounts2)
```

We loaded UMI counts data coming from `r length(unique(annot$run))` library/sequencing  runs and `r length(unique(annot$plate))` plates, for `r length(unique(annot$condition))` conditions.

The distribution of plates, conditions, and genotypes across runs is: 
`r table(annot$condition_plate, annot$nextseq500_run, useNA='ifany')  %>% knitr::kable()`. 



# Number of cells, UMIs, detected genes, ...

```{r nb reads and zeros, cache=FALSE}
nbUMIs <- colSums(umicounts[,-1])

non0genes <- colSums(umicounts[,-1] > 0)
       
only0genes <-  colSums(umicounts[,-1] == 0)

only0cells <-   colSums(umicounts[,-1] == 0) 

nb0genes <- colSums(umicounts[,-1] == 0)

prop0genes <- colSums(umicounts[,-1] == 0)/nrow(umicounts)

nbnot0genes <- colSums(umicounts[,-1] > 0)
       
```


## Number of detected genes across cells for each half plate

```{r, fig.width=10, fig.height=5}

dt0g <- data.table(nbZero = c(t(as.data.table(non0genes))),
           filename = gsub('\\.P.*', '', names(non0genes)),
           run=as.factor(annot$nextseq500_run), 
           condition = annot$condition) 
dt0g <- dt0g[order(dt0g$run,dt0g$filename, dt0g$nbZero),]

gg <- ggplot(dt0g, 
           aes(x = filename, y = nbZero, fill=run)) + 
      geom_bar(stat='identity',
               position=position_dodge2(1),
               size=.3,
               width = 0.5) +
        ylab( '#detected genes') +
      xlab('half plate') + 
      theme_bw( ) +
      theme( axis.text.x = element_text(angle = 90, size = 10, vjust = 0.5, hjust = 1), 
             axis.title.y = element_text(size = 10),
             axis.text.y = element_text(size = 10),
             axis.text.x.top = element_text(size = 10)) 
print(gg)

```



## Number of UMIs across cells for each half plate


```{r plot nb umis}

data.table(nbUMIs = c(t(as.data.table(nbUMIs))),
           filename = gsub('\\.P.*', '', names(nbUMIs)),
           run=as.factor(annot$nextseq500_run), 
           condition = annot$condition) %>% 
  .[order(run,filename,nbUMIs)] %>%  
  ggplot(.,aes(x = filename, y = nbUMIs, fill=run)) + 
      geom_bar(stat='identity',
               position=position_dodge2(1),
               size=.3,
               width = 0.5) +
        ylab( 'total UMI counts') +
      xlab('half plate') + 
      theme_bw( ) +
      theme( axis.text.x = element_text(angle = 90, size = 10, vjust = 0.5, hjust = 1), 
             axis.title.y = element_text(size = 10),
             axis.text.y = element_text(size = 10),
             axis.text.x.top = element_text(size = 10)) 


```


## Fraction of mitochondrial genes across cells for each half plate

```{r plot mito, fig.height=5}

mito <- grep(pattern = "^mt-", x = umicounts$gene, value = TRUE, ignore.case = TRUE)
propmito <- colSums(umicounts[umicounts$gene %in% mito,-1])/colSums(umicounts[,-1])

data.table(nbUMIs = c(t(as.data.table(propmito))),
           filename = gsub('\\.P.*', '', names(propmito)),
           run = as.factor(annot$nextseq500_run)) %>% 
  .[order(propmito,filename),] %>%  
  ggplot(., 
           aes(x = filename, y = nbUMIs, fill=run)) + 
      geom_bar(stat='identity',
               position=position_dodge2(1),
               size=.3,
               width = 0.5) +
        ylab( 'fraction of mitochondrial genes') +
      xlab('half plate') + 
      theme_bw( ) +
      theme( axis.text.x = element_text(angle = 90, size = 10, vjust = 0.5, hjust = 1), 
             axis.title.y = element_text(size = 10),
             axis.text.y = element_text(size = 10),
             axis.text.x.top = element_text(size = 10)) 

```


## Gene with largest UMI count per cell

This is to check whether there are genes that are predominant in a large number of cells.


```{r max gene, results='asis', cache.lazy=FALSE, cache.rebuild=TRUE}

# ignore ERCC spike-ins as they had very low counts and were not usable
umicountssel <- umicounts[grep('ERCC-', umicounts$gene, invert = TRUE),-1]
# ignore cells with zero counts to avoid dividing by zero
umicountssel <- umicountssel[,which(colSums(umicountssel)>0), with=F]


frac = scale(umicountssel, scale = colSums(umicountssel), center=FALSE)

frac <- as.data.table(frac)

frac <- frac[, gene := umicounts$gene[grep('ERCC-', umicounts$gene, invert = TRUE)]]
             

fracm <- melt(frac)
setnames(fracm, c('gene', 'cellid', 'frac'))

rm(frac)
# max gene per cell
maxfrac <- fracm[, list(genemax=gene[which.max(frac)], fracmax=frac[which.max(frac)]), by=c('cellid')]
maxfrac[, run := paste0('run',annot[match(maxfrac$cellid, annot$cellID),]$run)]
maxfrac[, plate := annot[match(maxfrac$cellid, annot$cellID),]$condition_plate]
maxfrac[, nbOcc := length(cellid), by = 'genemax']
maxfrac <- maxfrac[order(nbOcc, decreasing = TRUE),]

maxfrac_all <- maxfrac[, list(nbOcc = length(cellid)), by = 'genemax']

head(maxfrac_all, 20) %>% knitr::kable() %>% print()


```

There are `r sum(table(maxfrac$genemax)>=50)` genes that are predominant in at least 50 cells:  `r names(sort(table(maxfrac$genemax), decreasing=TRUE)[sort(table(maxfrac$genemax), decreasing=TRUE)>=50])`.

## Seurat violin plots

A Seurat object containing the unfiltered UMI counts is saved in the RData file `data/derived/YS/YS_umis.rda`.


```{r, fig.height=15, fig.width=9}
rownames(annot) <- annot$cellID

mcounts <- Matrix(as.matrix(umicounts[,-1]))
rownames(mcounts) <- umicounts$gene

umis <- CreateSeuratObject(counts = mcounts, 
                           meta.data = data.frame(annot, row.names = 'cellID'))

rm(mcounts)

mito <- grep(pattern = "^mt-", x = umicounts$gene, value = TRUE, ignore.case = TRUE)
propmito <- colSums(umicounts[umicounts$gene %in% mito,-1])/colSums(umicounts[,-1])
umis[['percent.mito']] <- propmito
umis$condition_plate <- paste(umis$condition, umis$plate, sep='_')
umis$condition_replicate_plate <- paste(umis$condition, umis$replicate, umis$plate, sep='_')
umis$condition_halfplate <- factor(umis$condition_halfplate, levels = unique(umis$condition_halfplate[order(umis$nextseq500_run)]))

VlnPlot(object = umis, features = c("nFeature_RNA", "nCount_RNA", "percent.mito"), 
        ncol = 1, group.by = 'condition_halfplate', pt.size = .05, 
        cols = annot$nextseq500_run[match(levels(umis$condition_halfplate), annot$condition_halfplate)]+1) 
                     #as.character(umis$nextseq500_run))

dir.create(paste0(dirdata,'/derived/YS/'))
save(umis, file=paste0(dirdata,'derived/YS/YS_umis.rda'))

```




## Histogram of number of detected genes 


```{r hist and scatter, fig.height=9, fig.width=10}
par(mfrow=c(4,5))
for (cp in unique(umis$condition_halfplate)) {
  hist(umis$nFeature_RNA[umis$condition_halfplate == cp], 
       main = cp, xlab='#detected genes', breaks=30)
  abline(v=2750, col = 'red', lty = 2)
}
```


## Number of UMIs versus number of detected genes 


```{r scatter, fig.height=9, fig.width=10}
par(mfrow=c(4,5))
for (cp in unique(umis$condition_halfplate)) {
  smoothScatter(x=umis$nFeature_RNA[umis$condition_halfplate == cp], 
                y=umis$nCount_RNA[umis$condition_halfplate == cp], 
       main = cp, xlab='#detected genes', ylab='#UMIs')#,
      # xlim = c(0,11000), ylim = c(0, 30000))
  abline(v=2750, col = 'red', lty = 2)
  #abline(v=2000, col = 'blue', lty = 2)
}

```



## Number of cells with at least 2,000 or 2,750 detected genes for each half plate and each condition


```{r, results=TRUE}



dt0g[, list(nbCells_atLeast2000genes =sum(nbZero > 2000), 
            nbCells_atLeast2750genes =sum(nbZero > 2750)), by = filename] %>% knitr::kable() %>% print()


dt0g[, list(nbCells_atLeast2000genes =sum(nbZero > 2000), 
            nbCells_atLeast2750genes =sum(nbZero > 2750)), by = condition] %>% knitr::kable() %>% print()


```


# Data filtering


## Filtering out cells with less than 2,750 detected genes

As a first selection, we filter out cells for which mitochondrial genes take up more than 5% of the total UMI counts, and that have less than 2,750 detected genes (i.e. less than 2,750 non-zero UMI counts). We keep genes that are detected in at least 5 cells.


```{r}
sceall <- CreateSeuratObject(count=umis@assays$RNA@counts,  min.cells=5, min.features =2750, meta.data = umis@meta.data)
sceall <- subset(sceall, subset = percent.mito < 0.05)
# remove spike-ins which are not usable anyway given their very low counts
sceall <- sceall[grep('^ERCC-', rownames(sceall), invert = TRUE), ]

sce <- as.SingleCellExperiment(sceall) 

```

 `r ncol(sceall)` cells  and `r nrow(sceall)` genes were selected .

## Doublet detection 

Doublet detection is done using the package `scDblFinder` using default parameters and running the analysis for each plate.
see https://www.biorxiv.org/content/10.1101/2020.02.02.930578v1.full.pdf,.

```{r doublet, eval=FALSE}
library(scDblFinder)
sce$groupForDoubletDetection <-  sce$plate
sce$groupForDoubletDetection[sce$nextseq500_run == 2] <-  'run2' #regroup run2 since there are very few cells after passing preliminary filtering
sce <- scDblFinder(sce, samples=sce$groupForDoubletDetection)
# how many cells are detected to be doublets ?
table(sce$scDblFinder.class)

```

```{r remove doublets, eval=FALSE}
sceall <- sceall[, sce$scDblFinder.class != 'doublet']
sce <- sce[, sce$scDblFinder.class != 'doublet']
```

<!-- Distribution of cells detected as doublets across plates by `scDblFinder`: r table(sce$scDblFinder.class, sce$condition_halfplate) %>% knitr::kable(). -->

<!-- We filter out r sum(sce$scDblFinder.class == 'doublet') cells detected as being doublets by `scDblFinder`. -->

Since we can't reproduce exactly the same selection of cells as the on in the paper (scDblFinder v1.1.8 doublet selection seems to vary a bit across runs of the same function). We saved the IDs of the selected cells in data/derived/YS and are loading them now.

We also discard plate 15 that only contains less than 10 cells passing the quality thresholds.

```{r}
sce <- sce[, sce$plate != '15']

# save cell selection
#selcells <- colnames(sce)
#saveRDS(selcells, file =paste0(dirdata,'derived/YS/YS_selcells.rds'))
selcells <- readRDS(file =paste0(dirdata,'derived/YS/YS_selcells.rds'))
sce <- sce[,selcells]
sceall <- sceall[,selcells]
```


## Number of cells after this first filtering step



The number of selected plates/cells for each plate is: `r table(sceall$condition_replicate_plate) %>% knitr::kable()`.
The number of cells selected per condition is: `r table(sceall$condition) %>% knitr::kable()`.
The number of cells selected per condition and genotype is: `r table(sceall$condition,sceall$genotype, useNA='ifany') %>% knitr::kable()`.

```{r}

#colData(sce) <- annot[match(colnames(sce), annot$cellID),]
save(sce, file=paste0(dirdata,'derived/YS/YS_sce.rda'))
```

The number of selected cells per replicate selected for each facs annotation is: `r table(sceall$FACS) %>% knitr::kable()`.

The UMI counts of the data remaining after this first filtering step are available in  `data/derived/YS/YS_sce.rda`.


